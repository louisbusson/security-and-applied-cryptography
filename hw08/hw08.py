#Louis BUSSON 545583
#The majority of key generation, signature and signature verifcation implementations 
#are inspired by examples provided in cryptography module documentation.
#The benchmark function is inspired by the implementation provided in the demo file during the seminar.

import os

from cryptography.hazmat.primitives.asymmetric import rsa, dsa, ec
from cryptography.hazmat.primitives.asymmetric import padding, utils
from cryptography.hazmat.primitives import hashes, hmac
from cryptography.exceptions import InvalidSignature

from timeit import default_timer as timer

READ_LEN = 1024

def main():
   task_1(2048, b"a secret message from alice")
   task_2(2048, b"a secret message from alice")
   task_3("alice.txt")
   task_4("alice.txt")
   #Task 6 : For the bonus question, I used the same task function with the big file path
   #task_4("big_file.txt")

def task_1(key_size, message):
   print("************** Task 1 **************")

   print("-------- RSA Signature scheme --------")
   print("Signature generation ...")
   rsa_private_key = rsa.generate_private_key(public_exponent=65537, key_size=key_size)
   rsa_signature = rsa_sign(rsa_private_key, hashes.SHA256(), message)
   print(rsa_signature)
   print("Signature verification ...")
   try:
      rsa_verify(rsa_private_key.public_key(), hashes.SHA256(), message, rsa_signature)
      print("RSA Valid Signature")
   except InvalidSignature: print("RSA Invalid Signature")
   
   print("\n-------- DSA Signature scheme --------")
   print("Signature generation ...")
   dsa_private_key = dsa.generate_private_key(key_size)
   dsa_signature = dsa_private_key.sign(
      message,
      hashes.SHA256()
   )
   print(dsa_signature)
   print("Signature verification ...")
   try:
      dsa_private_key.public_key().verify(
         dsa_signature,
         message,
         hashes.SHA256()
      )
      print("DSA Valid Signature")
   except InvalidSignature : print("DSA Invalid Signature")
   
   print("\n-------- EC Signature scheme --------")
   print("Signature generation ...")
   ec_private_key = ec.generate_private_key(
      curve=ec.SECP384R1()
   )
   ec_signature = ec_private_key.sign(
      message,
      ec.ECDSA(hashes.SHA256())
   )
   print(ec_signature)
   print("Signature verification ...")
   try :
      ec_private_key.public_key().verify(
         ec_signature,
         message,
         ec.ECDSA(hashes.SHA256())
      )
      print("EC Valid Signature")
   except InvalidSignature : print("EC Invalid Signature")


def task_2(key_size, message):
   print("\n************** Task 2 **************")
   key = os.urandom(key_size//8)
   
   print("SHA2 Signature generation ...")
   signature = hmac_sign(key, hashes.SHA256(), message)
   print(signature)
   print("Signature verification ...")
   try:
      hmac_verify(key, hashes.SHA256(), message, signature)
      print("HMAC SHA2 Valid Signature")
   except InvalidSignature : print("HMAC SHA2 Invalid Signature")

   print("SHA3 Signature generation ...")
   signature = hmac_sign(key, hashes.SHA3_256(), message)
   print(signature)
   print("Signature verification ...")
   try: 
      hmac_verify(key, hashes.SHA3_256(), message, signature)
      print("HMAC SHA3 Valid Signature")
   except InvalidSignature : print("HMAC SHA3 Invalid Signature")


def task_3(path):
   print("\n************** Task 3 **************")
   schemes = ["RSA", "DSA", "EC"]
   keys = list()
   keys.append(rsa.generate_private_key(public_exponent=65537, key_size=2048))
   keys.append(dsa.generate_private_key(2048))
   keys.append(ec.generate_private_key(curve=ec.SECP384R1()))
   sign_functions = [rsa_sign, dsa_sign, ec_sign, hmac_sign_file]
   verify_functions = [rsa_verify, dsa_verify, ec_verify, hmac_verify_file]
   for i in range(3):
      print("\n-------- "+schemes[i]+" Signature scheme --------")
      print("Signature generation ...")
      digest = prehash(path, hashes.SHA256())
      signature = sign_functions[i](keys[i], utils.Prehashed(hashes.SHA256()), digest)
      print(signature)
      print("Signature verification ...")
      try:
         verify_functions[i](keys[i].public_key(), utils.Prehashed(hashes.SHA256()), digest, signature)
         print(schemes[i]+" Valid signature")
      except InvalidSignature: print(schemes[i]+" Invalid signature")

   print("\n-------- HMAC Signature scheme --------")
   key = os.urandom(2048//8)
   print("Signature generation ...")
   signature = hmac_sign_file(key, hashes.SHA256(), path)
   print(signature)
   print("Signature verification ...")
   try:
      hmac_verify_file(key, hashes.SHA256(), path, signature)
      print("HMAC Valid Signature")
   except InvalidSignature: print("HMAC Invalid Signature")

def task_4(path):
   print("\n************** Task 4 **************")
   schemes = ["RSA", "DSA", "EC"]
   keys = list()
   sign_functions = [rsa_sign, dsa_sign, ec_sign, hmac_sign_file]
   verify_functions = [rsa_verify, dsa_verify, ec_verify, hmac_verify_file]
   print("-------- Keys Generation --------")
   print("RSA key generation time :", end=" ")
   keys.append(benchmark(rsa.generate_private_key, 65537, 3072))
   print("DSA key generation time :", end=" ")
   keys.append(benchmark(dsa.generate_private_key, 3072))
   print("EC key generation time :", end=" ")
   keys.append(benchmark(ec.generate_private_key, ec.SECP256R1()))
   print("Symetric key generation for HMAC time :", end=" ")
   keys.append(benchmark(os.urandom, 128//8))
   
   algorithms = { "SHA2": hashes.SHA256(), "SHA3": hashes.SHA3_256() }
   for algo in algorithms :
      signatures = list()
      
      print("\n-------- "+algo+" Signatures Generation --------")
      for i in range(3):
         print(schemes[i]+" signature time :", end=" ")
         digest = prehash(path, algorithms[algo]) 
         signatures.append(benchmark(sign_functions[i], keys[i], utils.Prehashed(algorithms[algo]), digest))
      print("HMAC signature time :", end="")
      signatures.append(benchmark(hmac_sign_file, keys[3], algorithms[algo], path))

      print("\n-------- "+algo+" Signatures Verification --------")
      for i in range(3):
         print(schemes[i]+" verification time :", end=" ")
         digest = prehash(path, algorithms[algo]) 
         benchmark(verify_functions[i], keys[i].public_key(), utils.Prehashed(algorithms[algo]), digest, signatures[i])
      print("HMAC verification time :", end="")
      try:
         benchmark(hmac_verify_file, keys[3], algorithms[algo], path, signatures[3])
      except InvalidSignature:
         print("Error : Invalid Signature ")


def benchmark(function, *args):
   MAX = 10
   overall = 0
   for i in range(MAX):
      start = timer()
      result = function(*args)
      end = timer()
      overall = overall + (end - start)

   print("%f" % (overall/MAX))
   return result


def prehash(path, algo):
   file = open(path, 'rb')
   data = file.read(READ_LEN)
   hasher = hashes.Hash(algo)
   
   while data:
      hasher.update(data)
      data = file.read(READ_LEN)
   
   file.close()
   return hasher.finalize()

def rsa_sign(key, algo, message):
   return key.sign(
      message,
      padding.PSS(
         mgf=padding.MGF1(hashes.SHA256()),
         salt_length=padding.PSS.MAX_LENGTH
      ),
      algo
   )

def rsa_verify(key, algo, message, signature):
   key.verify(
      signature,
      message,
      padding.PSS(
            mgf=padding.MGF1(hashes.SHA256()),
            salt_length=padding.PSS.MAX_LENGTH
      ),
      algo
   )

def dsa_sign(key, algo, message):
   return key.sign(
      message,
      algo
   )

def dsa_verify(key, algo,  message, signature):
   key.verify(
      signature,
      message,
      algo,
   )

def ec_sign(key, algo, message):
   return key.sign(
      message,
      ec.ECDSA(algo)
   )

def ec_verify(key, algo, message, signature):
   key.verify(
      signature,
      message,
      ec.ECDSA(algo)
   )
  
def hmac_sign(key, algo, message):
   hmac_object = hmac.HMAC(key, algo)
   hmac_object.update(message)
   return hmac_object.finalize()

def hmac_verify(key, algo, message, signature):
   hmac_object = hmac.HMAC(key, algo)
   hmac_object.update(message)
   hmac_object.verify(signature)
   

def hmac_sign_file(key, algo, path):
   file = open(path, 'rb')
   hmac_object = hmac.HMAC(key, algo)
   data = file.read(READ_LEN)
   while data:
      hmac_object.update(data)
      data = file.read(READ_LEN)
   file.close()
   return hmac_object.finalize()

def hmac_verify_file(key, algo, path, signature):
   file = open(path, 'rb')
   hmac_object = hmac.HMAC(key, algo)
   data = file.read(READ_LEN)
   while data:
      hmac_object.update(data)
      data = file.read(READ_LEN)
   file.close()
   hmac_object.verify(signature)

if __name__ == "__main__":
   main()