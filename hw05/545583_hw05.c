/*
 * This code compute the pdkf of IETF RFC-6070 test vectors using sha-1 and sha-256
 * It is inspired by the PKDF OpenSSL 3 example presented during the seminar.
 */
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <openssl/rand.h>
#include <openssl/kdf.h>
#include <openssl/core_names.h>
#include <openssl/crypto.h>

struct vector {
   char* password;
	int password_size;
   unsigned char* salt;
	unsigned long int salt_size;
   unsigned long int iterations;
   int dk_len;
};

/* Print buffer in HEX with optional separator */
static void hexprint(const unsigned char *d, int n, const char *sep)
{
	int i;

	for (i = 0; i < n; i++)
		printf("%02hhx%s", (const char)d[i], sep);
	printf("\n");
}

/* Convert hexadecimal string to an unsigned char array*/
unsigned char* hexToUnsignedChar(const char *hexString) {
	int len = strlen(hexString);
	if (len % 2 != 0) return NULL;

	int charCount = len / 2;
	unsigned char *result = calloc(charCount, 1);
	if (result == NULL) return NULL;

	for (int i = 0; i < charCount; i++) {
		if (sscanf(hexString + (2 * i), "%2hhx", &result[i]) != 1) {
			free(result);
			return NULL;
		}
	}

   return result;
}

int pkdf(char* hash, struct vector* test_vector, unsigned char ** key){
	
	OSSL_PARAM params[5], *p = params;
	EVP_KDF_CTX *ctx;
	EVP_KDF *pbkdf2;

	*key = calloc(test_vector->dk_len, 8);
	if (*key == NULL) 
		return 1;

	*p++ = OSSL_PARAM_construct_octet_string(OSSL_KDF_PARAM_PASSWORD, test_vector->password, test_vector->password_size);
	*p++ = OSSL_PARAM_construct_octet_string(OSSL_KDF_PARAM_SALT, test_vector->salt, test_vector->salt_size);
	*p++ = OSSL_PARAM_construct_ulong(OSSL_KDF_PARAM_ITER, &(test_vector->iterations));
	*p++ = OSSL_PARAM_construct_utf8_string(OSSL_KDF_PARAM_DIGEST, hash, 0);
	*p++ = OSSL_PARAM_construct_end();

	pbkdf2 = EVP_KDF_fetch(NULL, "pbkdf2", NULL);
	if (!pbkdf2)
		return 1;

	ctx = EVP_KDF_CTX_new(pbkdf2);
	if (!ctx)
		return 1;

	if (EVP_KDF_derive(ctx, *key, test_vector->dk_len, params) != 1)
		return 2;

	EVP_KDF_CTX_free(ctx);
	EVP_KDF_free(pbkdf2);

	hexprint(*key, test_vector->dk_len, " ");

	return 0;
}

int main(void)
{
	char *hash;
	int i;

	struct vector t_vectors[6];
	t_vectors[0] = (struct vector) {"password", 8, (unsigned char*) "salt", 4, 1, 20};
	t_vectors[1] = (struct vector) {"password", 8, (unsigned char*) "salt", 4, 2, 20};
	t_vectors[2] = (struct vector) {"password", 8, (unsigned char*) "salt", 4, 4096, 20};
	t_vectors[3] = (struct vector) {"password", 8, (unsigned char*) "salt", 4, 16777216, 20};
	t_vectors[4] = (struct vector) {"passwordPASSWORDpassword", 24, (unsigned char*) "saltSALTsaltSALTsaltSALTsaltSALTsalt", 36, 4096, 25};
	t_vectors[5] = (struct vector) {"pass\0word", 9, (unsigned char*) "sa\0lt", 5, 4096, 16};

	/*Expected values for sha-1*/
	char* expected_keys[6];
	expected_keys[0] = "0c60c80f961f0e71f3a9b524af6012062fe037a6";
	expected_keys[1] = "ea6c014dc72d6f8ccd1ed92ace1d41f0d8de8957";
	expected_keys[2] = "4b007901b765489abead49d926f721d065a429c1";
	expected_keys[3] = "eefe3d61cd4da4e4e9945b3d6ba2158c2634e984";
	expected_keys[4] = "3d2eec4fe41c849b80c8d83662c0e44a8b291a964cf2f07038";
	expected_keys[5] = "56fa6aa75548099dcc37d7f03425e0c3";

	hash = "sha1";
	for(i=0; i<6; i++){
		printf("Derived key using %i. vector PBKDF2-%s\n", i+1, hash);
		unsigned char* key;
		int result = pkdf(hash, t_vectors+i, &key);
		if (result != 0) {
			free(key);
			return result;
		}

		unsigned char* expected_key_hex_format = hexToUnsignedChar(expected_keys[i]);
		if (expected_key_hex_format == NULL) {
			free(key);
			return 1;
		}

		assert(!CRYPTO_memcmp(key, expected_key_hex_format, (t_vectors+i)->dk_len));
		free(key);
	}

	hash = "sha256";
	for(i=0; i<6; i++){
		printf("Derived key using %i. vector PBKDF2-%s\n", i+1, hash);
		(t_vectors+i)->dk_len = 32;
		unsigned char* key;
		int result = pkdf(hash, t_vectors+i, &key);
		free(key);
		if (result != 0) return result;
	}
	
	return 0;
}

