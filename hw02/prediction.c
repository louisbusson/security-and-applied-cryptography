#include <stdio.h>

int main(int argc, char *argv[]) {
   FILE *file;
   unsigned int buffer[1000];
   unsigned int contained[1000];
   unsigned int num;
   
   if(argc != 2) {
      printf("You need to specify a file\n");
      return 1;
   }

   file = fopen(argv[1], "rb");

   if (file == NULL) {
      perror("Error opening the file");
      return 1;
   }

   printf("First 31 numbers: \n");
   for (size_t i = 0; i < 31; i++) {
      fread(&num, sizeof(unsigned int), 1, file);
      buffer[i] = num << 1;
      printf("%u \t", (unsigned int) buffer[i]);
      if(i % 5 == 0) printf("\n");
   }

   printf("\n Following numbers predicted: \n");
   for (size_t j=31; j<100; j++) {
      buffer[j] = buffer[j-31] + buffer[j-3];
      printf("%u \t", ((unsigned int) buffer[j]) >> 1);
      if(j % 5 == 0) printf("\n");
   }

   printf("\n Numbers contained in file (must be the same): \n");
   for (size_t i = 0; i < 100; i++) {
      fread(&num, sizeof(unsigned int), 1, file);
      contained[i] = num;
      printf("%u \t", ((unsigned int) contained[i]));
      if(i % 5 == 0) printf("\n");
   }
   printf("\n");
   fclose(file);

   return 0;
}
