from locale import normalize
import os

def statistic(rnd_bytes, i, j):
   count = {'00': 0, '01':0, '10':0, '11':0 }
   normalize = bin(int(rnd_bytes.hex(), 16)).removeprefix('0b')
   for z in range(len(normalize)//8):
      byte = normalize[z*8:(z+1)*8]
      count[str(byte[i])+str(byte[j])] += 1

   for key in count:
      count[key] = round(count[key]/(len(normalize)//8)*100, 4)
   return count 

for i in range(1, 6):
    content = open(str(i)+'.bin', 'rb').read()
    print("File "+str(i)+".bin :")
    print(statistic(content, 0, 6))