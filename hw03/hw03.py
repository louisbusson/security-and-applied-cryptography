import datetime
import codecs
from cryptography import x509
from cryptography.x509 import NameOID
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes, aead
from cryptography.hazmat.primitives import hashes, asymmetric, serialization
from cryptography.hazmat.backends import default_backend

READ_LEN = 1024

#The generates_bob_csr() and provide_certificate() functions are inspired from the examples provided in the cryptography documentation.

def main():

    key_file = open("alice_key.txt", "r")
    key = codecs.decode(key_file.read(), "hex_codec")
    iv_file = open("alice_iv.txt", "r")
    iv = codecs.decode(iv_file.read(), "hex_codec")

    #Question 1 : Encrypt the file alice.txt with AES-128 in OFB mode
    encrypt(key, iv, "alice.txt")

    #Question 3 : Generate a private key for the CA
    CA_key = asymmetric.rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
    )

    generate_bob_csr()

    #Question 3 : Generate a certificate for Bob
    provide_certificate(CA_key, "bob_csr.pem")

    #Question 4 : Verify the certificate has been correctly signed by the CA
    verify_certificate(CA_key, "bob.com_certificate.pem")

def encrypt(key, iv, path):
    
    #Check if the key length is valid
    if len(key) != 16:
       print("Key length must be 128 bits !")
       return

    #Creates the cipher object with AES-128 in OFB mode
    cipher = Cipher(algorithms.AES128(key), modes.OFB(iv))

    #Opens the input file in binary reading format, this file contains the data to be encrypted
    input_file = open(path, 'rb')
    #Opens the file in binary writting format, this file will store encrypted data
    output_file = open(path.replace(".txt", "") + "_script_enc.txt", 'wb')  

    #First read to init the loop
    data = input_file.read(READ_LEN)

    #Loop to encrypt large files without memory issues
    while data:
        encryptor = cipher.encryptor()
        ct = encryptor.update(data) + encryptor.finalize()
        #Write encrypted data in output file
        output_file.write(ct)
        data = input_file.read(READ_LEN)

    #Closes the initial & encrypted data files
    output_file.close()
    input_file.close()


#This function aims to generate Bob's to test the following functions which provide and verify certificates
def generate_bob_csr():

    bob_key = asymmetric.rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
    )

    with open("bob_key.pem", "wb") as f:
        f.write(bob_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.NoEncryption(),
        ))
    
    bob_subject = x509.Name([
        x509.NameAttribute(NameOID.COUNTRY_NAME, u"CZ"),
        x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, u"Moravia"),
        x509.NameAttribute(NameOID.LOCALITY_NAME, u"Brno"),
        x509.NameAttribute(NameOID.ORGANIZATION_NAME, u"Bob's company"),
        x509.NameAttribute(NameOID.COMMON_NAME, u"bob.com"),
    ])
    bob_csr = x509.CertificateSigningRequestBuilder(subject_name=bob_subject).sign(bob_key, hashes.SHA256())

    with open("bob_csr.pem", "wb") as f:
        f.write(bob_csr.public_bytes(serialization.Encoding.PEM))
        f.close()


def provide_certificate(CA_key, csr_path):

    #Loads the CSR file    
    f= open(csr_path, "rb")
    csr = x509.load_pem_x509_csr(f.read(), default_backend())
    f.close()

    #Sets the issuer which is the Certification Authority
    issuer = x509.Name([
        x509.NameAttribute(NameOID.COUNTRY_NAME, u"US"),
        x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, u"Washington"),
        x509.NameAttribute(NameOID.LOCALITY_NAME, u"Redmond"),
        x509.NameAttribute(NameOID.ORGANIZATION_NAME, u"Microsoft CA"),
        x509.NameAttribute(NameOID.COMMON_NAME, u"microsoft.com"),
    ])

    #Creates the certificate
    cert = x509.CertificateBuilder(
        subject_name=csr.subject, #Uses the subject of the CSR (information about the entity which requests the certificate)
        issuer_name=issuer, #Uses the issuer defined above
        public_key=csr.public_key(), #Certies the public key of the entity which requests the certificate
        serial_number=x509.random_serial_number(), #Defines a random serial number
        not_valid_before=datetime.datetime.utcnow(), #The certificate is valid from now
        not_valid_after=datetime.datetime.utcnow() + datetime.timedelta(weeks=8), #The certificates expires in 8 weeks
    ).sign(CA_key, hashes.SHA256()) #The certificate is signed with the private key of the CA

    #Saves the certificate in a file
    name = csr.subject.get_attributes_for_oid(NameOID.COMMON_NAME)[0].value
    with open(name+"_certificate.pem", "wb") as f:
        f.write(cert.public_bytes(serialization.Encoding.PEM))
        f.close()


def verify_certificate(CA_key, cert_file_path):

    #Loads the certificate file
    with open(cert_file_path, "rb") as cert_file:
        cert_data = cert_file.read()
        cert_file.close()

    cert = x509.load_pem_x509_certificate(cert_data, default_backend())

    try:
        #Verifies the signature of the certificate with the public key of the CA
        CA_key.public_key().verify(
            cert.signature,
            cert.tbs_certificate_bytes,
            asymmetric.padding.PKCS1v15(),
            cert.signature_hash_algorithm,
        )
    except:
        print("Certificate signature is not valid.")
        return

    #Checks the beginning of the validity period
    if cert.not_valid_before > datetime.datetime.utcnow():
        print("Certificate is not valid yet.")
        return
    #Checks if the certificate is expired
    if cert.not_valid_after < datetime.datetime.utcnow():
        print("Certificate is expired.")
        return
    print("Certificate is valid.")


if __name__ == "__main__":
    main()