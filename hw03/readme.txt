Homework n°3 - BUSSON Louis Uco 545583 

############################################

1. OpenSSL commands :

Alice's key generation (128 bits to be used with AES 128 in python script) :
openssl rand -hex -out alice_key.txt 16

Alice's IV generation :
openssl rand -hex -out alice_iv.txt 16

Encryption of alice.txt using AES 128 in OFB mode with the key and IV generated above (result in alice_enc.txt) :
openssl enc -aes-128-ofb -in alice.txt -out alice_command_enc.txt -kfile alice_key.txt -iv 487f8f6068c2345eb6263c3568e8beb7 -nosalt -e

############################################

2. Decryption of alice_enc.txt using AES 128 in OFB mode using OpenSSL command:

openssl enc -aes-128-ofb -in alice_command_enc.txt -out alice_dec.txt -kfile alice_key.txt -iv 487f8f6068c2345eb6263c3568e8beb7 -nosalt -d

############################################

3. Verify the certificate and the key match using OpenSSL command:

openssl x509 -noout -modulus -in bob.com_certificate.pem | openssl sha256
openssl rsa -noout -modulus -in bob_key.pem | openssl sha256

The hash functions results must be the same for the certificate and the key.

############################################