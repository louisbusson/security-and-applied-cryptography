This repo contains the homeworks I produced for the **Laboratory and applied cryptography** course I took during my semester at Masaryk University.

The work focused on data encryption and hashing as well as electronic signatures and certificates.
Most of them use functions from the cryptography package in Python or the OpenSSL library in C.