/* Decode a base 64 file and decrypt its content */
/* This code is inspired by the example 7_bio_opensssl presented during the course*/
/* The use of BIO File functions is based on this example https://ladydebug.com/blog/2022/06/16/openssl-bio-api-file-saving-and-reading-examples/ */

#include <openssl/bio.h>
#include <openssl/evp.h>
#include <string.h>

const int BUF_SIZE = 1024;

/* Never ever hardcode secure keys in code ;-) */
const unsigned char KEY[] = {
   0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff
};

/* And also IV (Initialization vector) should be generated randomly... */
const unsigned char IV[]  = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f
};

/* Check if a string contains an ASCII character */
static int checkASCII(const char* text){
	if (text == NULL){
		printf("Cannot check ASCII characters in NULL string\n");
		return -1;
	}
	for (int i = 0; i < strlen(text); i++){
		if (text[i] < 0 || text[i] > 126){
			return 1;
		}
	}
	return 0;
}

static void decode_and_decrypt(const char* filename, const unsigned char *key, const unsigned char *iv)
{
	char* buffer;
	int bytes_read = 0;

	BIO *decipher = BIO_new(BIO_f_cipher());
	if(!decipher){
		printf("Cannot create BIO decipher\n");
		return;
	}

   BIO *b64 = BIO_new(BIO_f_base64());
	if(!b64){
		printf("Cannot create BIO b64\n");
		BIO_free_all(decipher);
		return;
	}

   BIO *file = BIO_new_file(filename, "r");
	if(!file){
		printf("Cannot open file %s\n", filename);
		BIO_free_all(decipher);
		return;
	}

	buffer = calloc(BUF_SIZE, sizeof(char));
	if(!buffer){
		printf("Cannot allocate memory for buffer\n");
		BIO_free_all(decipher);
		return;
	}

	/* Push BIOs in the stack and check for failures*/
	if(!BIO_push(b64, file) || !BIO_push(decipher, b64)){
		printf("Cannot push BIOs\n");
		BIO_free_all(decipher);
		free(buffer);
		return;
	}

	if(!BIO_set_cipher(decipher, EVP_aes_128_cbc(), key, iv, 0)){
		printf("Cannot set cipher\n");
		BIO_free_all(decipher);
		free(buffer);
		return;
	}

	while ((bytes_read = BIO_read(decipher, buffer, BUF_SIZE-1)) > 0) {
		if(bytes_read < 0){
			printf("Cannot read from BIO decipher\n");
			BIO_free_all(decipher);
			free(buffer);
			return;
		}
		if(checkASCII(buffer)){
			printf("This file contains non-ASCII characters\n");
			BIO_free_all(decipher);
			free(buffer);
			return;
		}
		
		printf("%s", buffer);
		memset(buffer, 0, BUF_SIZE);
	}
   
	free(buffer);
	BIO_free_all(decipher);

}

int main(int argc, char **argv)
{
	if(argc == 2){
		decode_and_decrypt(argv[1], KEY, IV);
		return 0;
	}
	else {
		printf("Usage: %s <filename>\n", argv[0]);
		return -1;
	}
}
